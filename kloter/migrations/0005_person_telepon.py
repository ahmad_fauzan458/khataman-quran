# Generated by Django 2.2.6 on 2021-11-18 10:44

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('kloter', '0004_auto_20211118_1237'),
    ]

    operations = [
        migrations.AddField(
            model_name='person',
            name='telepon',
            field=models.TextField(default=django.utils.timezone.now, max_length=20),
            preserve_default=False,
        ),
    ]
