from django.http import HttpResponse
from django.http import response
from django.http.response import FileResponse
from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import NumberKloter
from .models import Person
from .forms import PersonForm
from datetime import date


def index(request):
    response = {}
    number = len(NumberKloter.objects.all())
    response['kloter_number'] = str(number)
    return render(request, 'dashboard.html', response)

def list_kloter(request):
    response = {}
    number = NumberKloter.objects.all()
    response['kloter_number2'] = number
    return render(request, 'listkloter.html', response)

def addkloter(request):
    new_kloter = NumberKloter(kloter_number=len(NumberKloter.objects.all())+1, ronde=1)
    new_kloter.save()
    return HttpResponseRedirect('/kloter/')

def render_kloter(request, kloter):
    response = {}
    persons = Person.objects.filter(nomer_kloter=kloter)
    persons = [obj for obj in persons]
    persons_selesai = Person.objects.filter(selesai=True, nomer_kloter=kloter)
    persons_belum_selesai = Person.objects.filter(selesai=False, nomer_kloter=kloter)
    persons_belum_selesai = [obj for obj in persons_belum_selesai]
    jumlah_selesai = 0
    for i in persons:
        if i.selesai:
            jumlah_selesai = jumlah_selesai + 1

    persons.sort(key=lambda x: x.no_juz)
    persons_belum_selesai.sort(key=lambda x: x.no_juz)

    response['selesai'] = jumlah_selesai
    list_number = [i for i in range(1, len(persons)+1)]
    periode = NumberKloter.objects.get(kloter_number=kloter)
    response['persons'] = persons
    response['persons_selesai'] = persons_selesai
    response['persons_belum_selesai'] = persons_belum_selesai
    list_number_belum_selesai = [i for i in range(1, len(persons_belum_selesai)+1)]
    response['list_number_belum_selesai'] = list_number_belum_selesai
    response['list_number'] = list_number
    response['periode'] = periode
    response['tanggal'] = date.today()

    return render(request, 'kloter.html', response)


def new_juz(request, kloter):
    periode = NumberKloter.objects.get(kloter_number=kloter)
    periode.ronde = periode.ronde + 1
    periode.save()
    persons = Person.objects.filter(nomer_kloter=int(kloter))
    persons = [obj for obj in persons]
    persons.sort(key=lambda x: x.no_juz)
    for i in persons:
        i.selesai = False
        i.save()
    temp_name = ""
    temp_no_telp = ""
    if len(persons) > 2:
        temp_name = persons[0].name
        temp_no_telp = persons[0].telepon
        persons[0].name = persons[len(persons)-1].name
        persons[0].telepon = persons[len(persons)-1].telepon
        persons[0].save()
        for i in range(len(persons)-1):
            temp1 = persons[i+1].name
            temp2 = persons[i+1].telepon

            persons[i+1].name = temp_name
            persons[i+1].telepon = temp_no_telp
            persons[i+1].save()
            temp_name = temp1
            temp_no_telp = temp2



            
    

    return HttpResponseRedirect('/' + str(kloter))

def formperson(request):
    response = {}
    response['person_form'] = PersonForm
    return render(request, 'formtambah.html', response)

def addperson(request):
    response = {}
    form = PersonForm(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        response['no_juz'] = int(request.POST['no_juz'])
        response['nomer_kloter'] = int(request.POST['nomer_kloter'])
        response['telepon'] = request.POST['telepon']
        periode = NumberKloter.objects.get(kloter_number=response['nomer_kloter'])
        person = Person(name=response['name'], no_juz=response['no_juz'], nomer_kloter=response['nomer_kloter'], ronde=periode.ronde, telepon=response['telepon'])
        person.save()
        return HttpResponseRedirect('/' + str(response['nomer_kloter']))
    else:
        return HttpResponseRedirect('/')

def editperson(request, id):
    response = {}
    response['no_id'] = id
    obj = Person.objects.get(id=id)
    response['data_person'] = obj
    form = PersonForm(initial={'name': obj.name, 'no_juz': obj.no_juz, 'nomer_kloter': obj.nomer_kloter, 'telepon': obj.telepon})
    response['person_form'] = form
    return render(request, 'editperson.html', response)

def updateperson(request, id):
    response = {}
    form = PersonForm(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        response['no_juz'] = int(request.POST['no_juz'])
        response['nomer_kloter'] = int(request.POST['nomer_kloter'])
        response['telepon'] = request.POST['telepon']
        person = Person.objects.get(id=id)
        person.name = response['name']
        person.no_juz = response['no_juz']
        person.nomer_kloter = response['nomer_kloter']
        person.ronde = response['nomer_kloter']
        person.telepon = response['telepon']
        person.save()
        return HttpResponseRedirect('/' + str(response['nomer_kloter']))
        
    else:
        return HttpResponseRedirect('/')

def deleteperson(request, id):
    response={}
    person = Person.objects.get(id = id)
    kloter = person.nomer_kloter
    person.delete()
    return HttpResponseRedirect('/' + str(kloter))

def resetjuz(request, kloter):
    persons = Person.objects.filter(nomer_kloter=int(kloter))
    list_number = [i for i in range(1, len(persons)+1)]
    for i in range(len(persons)):
        t = persons[i]
        t.no_juz = list_number[i]
        t.save()
    return HttpResponseRedirect('/' + str(kloter))

def resetputaran(request, kloter):
    putaran = NumberKloter.objects.filter(kloter_number=int(kloter))
    persons = Person.objects.filter(nomer_kloter=int(kloter))
    for i in range(len(persons)):
        x = persons[i]
        x.selesai = False
        x.save()
    t = putaran[0]
    t.ronde = 1
    t.save()

    return HttpResponseRedirect('/' + str(kloter))


def selesai(request, id):
    person = Person.objects.get(id = id)
    kloter = person.nomer_kloter
    persons = Person.objects.filter(nomer_kloter=int(kloter))
    for i in range(len(persons)):
        if persons[i].id == id:
            t = persons[i]
            t.selesai = True
            t.save()
        else:
            last = persons[i].selesai
            t = persons[i]
            t.selesai = last
            t.save()
    return HttpResponseRedirect('/' + str(kloter))

#belum
def belumselesai(request, id):
    person = Person.objects.get(id = id)
    kloter = person.nomer_kloter
    persons = Person.objects.filter(nomer_kloter=int(kloter))
    for i in range(len(persons)):
        if persons[i].id == person.id:
            t = persons[i]
            t.selesai = False
            t.save()
        else:
            last = persons[i].selesai
            t = persons[i]
            t.selesai = last
            t.save()
    return HttpResponseRedirect('/' + str(kloter))
