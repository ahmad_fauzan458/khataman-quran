from django.contrib import admin

from .models import NumberKloter
from .models import Person


admin.site.register(NumberKloter)
admin.site.register(Person)
