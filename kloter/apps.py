from django.apps import AppConfig


class KloterConfig(AppConfig):
    name = 'kloter'
